const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require('morgan');
const path = require("path");
const { port = 3000 } = process.env;
const { v4: uuidv4 } = require('uuid'); // For generating room tokens

const {
  sequelize,
  User_Game,
  User_Game_Biodata,
  User_Game_History,
  Player,
  Room
} = require("./models");
const passport = require("./lib/passport")
const flash = require('express-flash')
const session = require('express-session')
// let users = require("./db/users.json");
// const flash = require('express-flash')

app.use(express.json());
let players = {}; // Store registered players
let rooms = {};   // Store created rooms

app.use(session({
  secret: "secret_key",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// routes
const userProfile = require("./routes/userprofile");
const errorHandler = require("./routes/errorhandler");
const indexRoutes = require("./routes/indexRouter");
const loginRoutes = require("./routes/loginRouter");
const signupRoutes = require("./routes/signupRouter");
const dashboardRoutes = require("./routes/dashboardRouter");

// can't login message on login.ejs
app.locals.message = null;

app.set("view engine", "ejs");

app.use(morgan('combined'));

app.use(express.static(path.join(__dirname, "public")));

app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());



// routes for static page (home, work, aboutme, contact)
app.use(indexRoutes);
app.use(loginRoutes);
app.use(signupRoutes);
app.use(dashboardRoutes);

// // static login method
// app.post("/users", async (req, res) => {
//   const { email, password } = req.body;

//   const user = users.find(
//     (user) => user.email === email && user.password === password
//   );

//   const superuser = users.find(
//     (superuser) =>
//       superuser.email === email &&
//       superuser.password === password &&
//       superuser.role === "admin"
//   );

//   if (!user) {
//     res.status(401).render("login", { message: "Wrong Email or Password!" });
//   } else if (superuser) {
//     try {
//       const user_game = await User_Game.findAll();
//       const user_game_biodata = await User_Game_Biodata.findAll();
//       const user_game_history = await User_Game_History.findAll();
//       res.render("dashboard", {
//         user_game,
//         user_game_biodata,
//         user_game_history,
//       });
//     } catch (err) {
//       console.error("Error retrieving data:", err);
//       res.status(500).send("Internal Server Error");
//     }
//   } else {
//     const { username } = user;
//     res.render("greet", { username });
//   }
// });

// create new user
// app.post("/users/create", async (req, res) => {
//   try {
//     const userGame = await User_Game.create({
//       username: req.body.username,
//       email: req.body.email,
//       password: req.body.password,
//       role: req.body.role,
//     });
//     await User_Game_Biodata.create({
//       first_name: req.body.firstname,
//       last_name: req.body.lastname,
//       gender: req.body.gender,
//       country: req.body.country,
//       user_id: userGame.id,
//       date_of_birth: req.body.birthday,
//     });
//     const user_game = await User_Game.findAll();
//     const user_game_biodata = await User_Game_Biodata.findAll();
//     const user_game_history = await User_Game_History.findAll();
//     res.render("dashboard", {
//       user_game,
//       user_game_biodata,
//       user_game_history,
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Terjadi kesalahan dalam membuat pengguna");
//   }
// });

app.post('/player', async (req, res) => {
  const { username, password } = req.body;

  try {
    const existingPlayer = await Player.findOne({ where: { username } });
    if (existingPlayer) {
      return res.status(400).json({ message: 'Username already exists' });
    }

    const newPlayer = await Player.create({ username, password, wins: 0, losses: 0, draws: 0 });

    return res.json({ message: 'Registration successful', player: newPlayer });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});



app.post('/login-player', async (req, res) => {
  const { username, password } = req.body;

  try {
    const player = await Player.findOne({ where: { username } });

    if (!player || player.password !== password) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }

    return res.json({ message: 'Login successful' });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});

app.post('/create-room', async (req, res) => {
  const { username } = req.body;

  try {
    // Check if the player is logged in
    const player = await Player.findOne({ where: { username } });
    if (!player) {
      return res.status(401).json({ message: 'You must be logged in to create a room' });
    }

    // Generate a unique room token
    const roomToken = uuidv4();

    // Create a new room entry in the database
    const newRoom = await Room.create({
      token: roomToken,
      players: [username], // Include the current player in the players array
      choices: {},
      gamesPlayed: 0,
    });

    return res.json({ roomToken });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});

app.post('/join-room', async (req, res) => {
  const { roomToken, username } = req.body;

  try {
    // Find the room in the database
    const room = await Room.findOne({ where: { token: roomToken } });
    if (!room) {
      return res.status(404).json({ message: 'Room not found' });
    }

    if (room.players.length >= 2) {
      return res.status(400).json({ message: 'Room is full' });
    }

    // Update the room in the database to add the player
    await room.update({ players: [...room.players, username] });

    return res.json({ message: 'Joined room successfully' });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});

app.post('/make-choice', async (req, res) => {
  const { roomToken, username, choice } = req.body;

  try {
    // Find the room in the database
    const room = await Room.findOne({ where: { token: roomToken } });
    if (!room) {
      return res.status(404).json({ message: 'Room not found' });
    }

    // Check if the player is in the room
    if (!room.players.includes(username)) {
      return res.status(403).json({ message: 'You are not in this room' });
    }

    // Check if maximum games played
    if (room.gamesPlayed >= 3) {
      return res.status(400).json({ message: 'Maximum games played' });
    }

    // Update the room's choices in the database
    const choices = { ...room.choices, [username]: choice };
    await room.update({ choices });

    // Check if both players have made their choices
    if (Object.keys(choices).length === 2) {
      const player1Choice = choices[room.players[0]];
      const player2Choice = choices[room.players[1]];
      let result = null;

      // Calculate the result
      if (player1Choice === player2Choice) {
        result = 'draw';
        // Update player stats in the database
        await Promise.all([
          Player.increment('draws', { where: { username: room.players[0] } }),
          Player.increment('draws', { where: { username: room.players[1] } }),
        ]);
      } else if (
        (player1Choice === 'rock' && player2Choice === 'scissors') ||
        (player1Choice === 'scissors' && player2Choice === 'paper') ||
        (player1Choice === 'paper' && player2Choice === 'rock')
      ) {
        result = 'player 1 wins';
        // Update player stats in the database
        await Promise.all([
          Player.increment('wins', { where: { username: room.players[0] } }),
          Player.increment('losses', { where: { username: room.players[1] } }),
        ]);
      } else {
        result = 'player 2 wins';
        // Update player stats in the database
        await Promise.all([
          Player.increment('wins', { where: { username: room.players[1] } }),
          Player.increment('losses', { where: { username: room.players[0] } }),
        ]);
      }

      // Increment gamesPlayed and clear choices for the next round in the database
      await room.increment('gamesPlayed');
      await room.update({ choices: {} });

      return res.json({ result });
    } else {
      return res.json({ message: 'Choice recorded' });
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});

app.get('/scoreboard/:username', async (req, res) => {
  const { username } = req.params;

  try {
    // Find the player in the database
    const player = await Player.findOne({ where: { username } });
    if (!player) {
      return res.status(404).json({ message: 'Player not found' });
    }

    return res.json({
      wins: player.wins,
      losses: player.losses,
      draws: player.draws,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
});




// edit user form
app.get("/users/edit/:uuid", async (req, res) => {
  try {
    const user_game = await User_Game.findOne({
      where: { uuid: req.params.uuid },
      include: [{ model: User_Game_Biodata, as: "biodata" }],
    });
    if (!user_game) {
      return res.status(404).send("User not found");
    }
    const user_game_biodata = user_game.biodata;
    res.render("edit", {
      user_game,
      user_game_biodata,
    });
  } catch (err) {
    console.error("Error fetching user data:", err);
    res.status(500).send("Internal Server Error");
  }
});

// update user
app.post("/users/edit/update/:uuid", async (req, res) => {
  const {
    username,
    email,
    password,
    role,
    firstname,
    lastname,
    gender,
    country,
    birthday,
  } = req.body;
  try {
    const [numRowsUpdated, [userGame]] = await User_Game.update(
      {
        username: username,
        email: email,
        password: password,
        role: role,
      },
      {
        where: { uuid: req.params.uuid },
        returning: true,
      }
    );
    if (numRowsUpdated !== 1) {
      throw new Error("User not found or not updated");
    }
    await User_Game_Biodata.update(
      {
        first_name: firstname,
        last_name: lastname,
        gender: gender,
        country: country,
        date_of_birth: birthday,
      },
      {
        where: { user_id: userGame.id },
      }
    );
    const user_game = await User_Game.findAll();
    const user_game_biodata = await User_Game_Biodata.findAll();
    const user_game_history = await User_Game_History.findAll();
    res.render("dashboard", {
      user_game,
      user_game_biodata,
      user_game_history,
    });
  } catch (err) {
    res.status(500).json("Can't update the user");
  }
});

// delete user
app.post("/users/delete/:uuid", async (req, res) => {
  try {
    await User_Game.destroy({
      where: { uuid: req.params.uuid },
    });
    const user_game = await User_Game.findAll();
    const user_game_biodata = await User_Game_Biodata.findAll();
    const user_game_history = await User_Game_History.findAll();
    res.render("dashboard", {
      user_game,
      user_game_biodata,
      user_game_history,
    });
  } catch (err) {
    console.error("Error deleting user:", err);
    res
      .status(500)
      .send(
        "An error occurred while deleting the user and associated biodata."
      );
  }
});

// user profile route
app.use(userProfile);

// error handler route
app.use(errorHandler);

// Port Listen
app.listen(port, async () => {
  console.log(`app listening to port ${port}`);
  await sequelize.authenticate();
  console.log("database connected bro!");
});




// CODE BELOW FOR TESTING PURPOSES ONLY!

// app.post("/test/users/create", async (req, res) => {
//   try {
//     const userGame = await User_Game.create({
//       username: req.body.username,
//       email: req.body.email,
//       password: req.body.password,
//       role: req.body.role,
//     });
//     await User_Game_Biodata.create({
//       first_name: req.body.firstname,
//       last_name: req.body.lastname,
//       gender: req.body.gender,
//       country: req.body.country,
//       user_id: userGame.id,
//       date_of_birth: req.body.birthday,
//     });
//     await User_Game_History.create({
//       win: req.body.win,
//       lose: req.body.lose,
//       total_match: req.body.totalmatch,
//       user_id: userGame.id,
//     });
//     res.json(userGame);
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Terjadi kesalahan dalam membuat pengguna");
//   }
// });

// app.get("/test/datas", async (req, res) => {
//   try {
//     const userGame = await User_Game.findAll({
//       include: [
//         { model: User_Game_Biodata, as: "biodata" },
//         { model: User_Game_History, as: "history" },
//       ],
//     });
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });

// app.post("/test/post", async (req, res) => {
//   try {
//     const userGame = await User_Game.create(req.body);
//     await User_Game_Biodata.create(req.body);
//     await User_Game_History.create(req.body);
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });

// app.get("/test", async (req, res) => {
//   try {
//     const userGame = await User_Game.findAll({
//       include: [{ model: User_Game_Biodata, as: "biodata" }],
//     });
//     res.json(userGame);
//   } catch (err) {
//     res.status(500).json(error);
//   }
// });