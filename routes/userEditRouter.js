const router = require('express' ).Router()
const userEditController = require('../controllers/indexController').userEdit

router.get("/users/edit/:uuid", userEditController.userEdit);

router.post("/users/edit/update/:uuid", userEditController.userEdit);

module.exports = router;