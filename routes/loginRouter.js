const router = require('express').Router();
const loginController = require('../controllers/indexController').login;
const restrict = require('../middlewares/restrict').restrict;

router.get('/login', loginController.login);

// CREATE
// router.post("/login", loginController.authLogin);

//router.post('/login', loginController.adminLogin);
router.post('/login', loginController.userLogin);

router.get('/', restrict, (req, res) => res.render('index'));

router.get('/user', restrict, loginController.profile);

// router.post('/login', passport.authenticate('local', {
//   successRedirect: '/',
//   failureRedirect: '/login',
//   failureFlash: true
//   }))
router.get('/profile', restrict, loginController.profile);
router.post('/api/v1/auth/login', loginController.login);
router.get('/api/v1/auth/profile', restrict, loginController.profile);
module.exports = router;
