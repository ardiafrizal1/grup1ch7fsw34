const router = require("express").Router();
const dashboardController = require("../controllers/indexController").dashboard;

router.get("/dashboard", dashboardController.userData);

// CREATE
router.post("/users/create", dashboardController.userCreate);

router. post("/users/delete/:uuid", dashboardController.userDelete);



  module.exports = router;