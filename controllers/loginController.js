const { sequelize, User_Game, User_Game_Biodata, User_Game_History } = require('../models');
const passport = require('../lib/passport');

function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}
const login = (req, res) => {
  res.render('login');
};

const userLogin = (req, res) => {
  User_Game.authenticate(req.body).then((user) => {
    res.json(format(user));
  });
};
const adminLogin = async (req, res) => {
  const { username, password } = await User_Game.authenticate(req.body);
  const superuser = await User_Game.findOne({
    where: {
      username: username,
      password: password,
      role: 'superadmin',
    },
  });
  try {
    if (superuser) {
      const user_game = await User_Game.findAll();
      const user_game_biodata = await User_Game_Biodata.findAll();
      const user_game_history = await User_Game_History.findAll();

      // const token = createToken(user_game[0].id); // Assuming user_game is an array

      // res.cookie('jwt', token, { maxAge: maxAge * 1000 });
      // res.status(200).json({ user: user_game[0].id }); // Assuming user_game is an array

      return res.render('dashboard', {
        user_game,
        user_game_biodata,
        user_game_history,
      });
    } else {
      return res.status(401).render('login', { message: 'Wrong Email or Password!' });
    }
  } catch (err) {
    console.error('Error retrieving data:', err);
    return res.status(500).send('Internal Server Error');
  }
};

const profile = (req, res) => {
  const currentUser = req.user;
  res.json(currentUser);
};

module.exports = { login, adminLogin, profile, userLogin };
