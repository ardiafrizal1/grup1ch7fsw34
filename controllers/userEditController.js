const { User_Game, User_Game_Biodata } = require('../models' );


// app.get("/users/edit/:uuid", 

const userEdit = async (req, res) => {
    try {
      const user_game = await User_Game.findOne({
        where: { uuid: req.params.uuid },
        include: [{ model: User_Game_Biodata, as: "biodata" }],
      });
      if (!user_game) {
        return res.status(404).send("User not found");
      }
      const user_game_biodata = user_game.biodata;
      res.render("edit", {
        user_game,
        user_game_biodata,
      });
    } catch (err) {
      console.error("Error fetching user data:", err);
      res.status(500).send("Internal Server Error");
    }
  };

  
// update user
// app.post("/users/edit/update/:uuid",

const userUpdate = async (req, res) => {
  const {
    username,
    email,
    password,
    role,
    firstname,
    lastname,
    gender,
    country,
    birthday,
  } = req.body;
  try {
    const [numRowsUpdated, [userGame]] = await User_Game.update(
      {
        username: username,
        email: email,
        password: password,
        role: role,
      },
      {
        where: { uuid: req.params.uuid },
        returning: true,
      }
    );
    if (numRowsUpdated !== 1) {
      throw new Error("User not found or not updated");
    }
    await User_Game_Biodata.update(
      {
        first_name: firstname,
        last_name: lastname,
        gender: gender,
        country: country,
        date_of_birth: birthday,
      },
      {
        where: { user_id: userGame.id },
      }
    );
    const user_game = await User_Game.findAll();
    const user_game_biodata = await User_Game_Biodata.findAll();
    const user_game_history = await User_Game_History.findAll();
    res.render("dashboard", {
      user_game,
      user_game_biodata,
      user_game_history,
    });
  } catch (err) {
    res.status(500).json("Can't update the user");
  }
};


module.exports = { userEdit, userUpdate}