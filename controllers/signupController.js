const { User_Game, User_Game_Biodata } = require('../models' );
const passport = require('../lib/passport' );
const bcrypt = require('bcrypt')

const getSignup = (req, res) => {
    res.render("signup");
  };

const authSignup = async (req, res) => {
  const {
    username,
    email,
    password,
    role,
    firstname,
    lastname,
    gender,
    country,
    birthday,
  } = req.body;
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt)
  try {
    const userGame = await User_Game.create({
      username: username,
      email: email,
      password: hashPassword,
      role: role,
    });
    await User_Game_Biodata.create({
      first_name: firstname,
      last_name: lastname,
      gender: gender,
      country: country,
      user_id: userGame.id,
      date_of_birth: birthday,
    });
    res.redirect("/login");
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
};

  module.exports = { getSignup, authSignup }
  