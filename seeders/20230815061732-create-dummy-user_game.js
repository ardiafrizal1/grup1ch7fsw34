'use strict';
const bcrypt = require('bcrypt');
const {
  sequelize,
  User_Game,
  User_Game_Biodata,
  User_Game_History,
} = require("../models");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const salt = await bcrypt.genSalt();
    const password = "admin";
    const hashPassword = await bcrypt.hash(password, salt);

    await queryInterface.bulkInsert(
      "user_game",
      [
        {
          uuid: "5d6cf7b0-9a6d-45d0-8080-afd22b3ed1fc",
          username: "admin",
          email: "admin@mail.com",
          password: hashPassword,
          role: "superadmin",
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );

    const userGame = await User_Game.findOne({
      where: {
          role: 'superadmin',
      },
  });

  return await queryInterface.bulkInsert('user_game_biodata', [{
    
    uuid: "5d6cf7b0-9a6d-45d0-8080-afd22b3ed1fc",
    user_id: userGame.id,
    first_name: "admin",
    last_name: "admin",
    gender: "male",
    date_of_birth: "2003-08-15T07:27:43.155Z",
    country: "Indonesia",
    created_at: new Date(),
    updated_at: new Date(),
}], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
