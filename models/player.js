'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Player extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Player.init({
    username: {
      type: DataTypes.STRING,
      allowNull : false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull : false,
    },
    wins: {
      type: DataTypes.INTEGER,
      allowNull: false, // Set allowNull to false for wins column
      defaultValue: 0,
    },
    losses: {
      type: DataTypes.INTEGER,
      allowNull: false, // You can also set allowNull to false for other columns if needed
      defaultValue: 0,
    },
    draws: {
      type: DataTypes.INTEGER,
      allowNull: false, // Set allowNull to false for draws column
      defaultValue: 0,
    },
  }, {
    sequelize,
    tableName: 'players',
    modelName: 'Player',
  });
  return Player;
};