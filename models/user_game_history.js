"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_Game_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User_Game, {
        foreignKey: "user_id",
        as: "user",
      });
    }
  }
  User_Game_History.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      uuid: {
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        references: {
          model: "user_game",
          key: "id",
        },
      },
      win: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      lose: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      total_match: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "User_Game_History",
      tableName: "user_game_history",
      underscored: true,
    }
  );
  return User_Game_History;
};
