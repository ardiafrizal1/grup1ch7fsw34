"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class User_Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.User_Game_Biodata, {
        foreignKey: "user_id",
        as: "biodata",
      });
      this.hasOne(models.User_Game_History, {
        foreignKey: "user_id",
        as: "history",
      });
    }

    // encryption method
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    // signup method
    static signup = ({ username, email, password }) => {
      const encryptedPassword = this.#encrypt(password);

      return this.create({ username, email, password: encryptedPassword });
    };

    checkPassword = password => bcrypt.compareSync(password, this.password)
generateToken = () => {
const payload = {
id: this.id,
username: this.username
}
const secret = 'secret_key'
const token = jwt.sign(payload, secret)
return token
};

/* Method Authenticate, untuk login */
static authenticate = async ({ username, password }) => {
  try {
  const user = await this.findOne({ where : { username }})
  if (!user) return Promise.reject("User not found!" )
  const isPasswordValid = user.checkPassword (password)
  if (!isPasswordValid) return Promise.reject("Wrong password" )
  return Promise.resolve(user)
  }
  catch(err) {
  return Promise.reject(err)
  }
  };

  }
  User_Game.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      uuid: {
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "Input a username!" },
          notEmpty: { msg: "Username must not be empty" },
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "Input an email!" },
          notEmpty: { msg: "Email must not be empty" },
          isEmail: { msg: "Must be a valid email address" },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: "Input a password!" },
          notEmpty: { msg: "Password must not be empty" },
        },
      },
      role: {
        type: DataTypes.ENUM("User", "Beta Tester"),
        defaultValue: "User",
        allowNull: false,
        validate: {
          notNull: { msg: "user must have a role" },
          notEmpty: { msg: "Role must not be empty" },
        },
      },
    },
    {
      sequelize,
      modelName: "User_Game",
      tableName: "user_game",
      underscored: true,
    }
  );
  return User_Game;
};
