'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define associations here
      Room.belongsToMany(models.Player, { through: 'RoomPlayer' });
    }
  }
  Room.init({
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    players: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      defaultValue: [],
    },
    choices: {
      type: DataTypes.JSONB,
      defaultValue: {},
    },
    gamesPlayed: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  }, {
    sequelize,
    tableName: 'rooms',
    modelName: 'Room',
  });
  return Room;
};
